var express = require('express');
var router = express.Router();
const CallAPIs = require('../public/javascripts/call_apis');
const ResponseData = require('../public/javascripts/response_message');
const template_json_message = require('../template.json');

/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index', { title: 'Express' });
});

router.post('/hoge', (req, res) => {
  console.log(req);
  res.send('ok');
  return;
});


router.post('/webhook', async (req, res) => {
  // reply to line
  console.log(req.body.events[0]);
  await CallAPIs.lineMessaging(req.body.events[0].replyToken, template_json_message);
  return;
  // console.log(res.body);
});

module.exports = router;
