require('dotenv').config();
const axios = require('axios');

module.exports = {
  lineMessaging: async (replyToken, extract) => {
    const data = {
      replyToken: replyToken,
      messages: [{
        type: 'flex',
        altText: "This is a Flex Message",
        contents: extract
      }]
    };

    const headers = {
        'Content-Type': 'application/json',
        'Authorization': `Bearer ${process.env.LINE_CHANNEL_ACCESS_TOKEN}`
    };

    await axios.post('https://api.line.me/v2/bot/message/reply', data, {headers: headers});
  }

};
